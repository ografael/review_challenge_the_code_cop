# Eu ignorei a estrutura de pacote dessa classe pra ficar mais fácil a visualização
# da solução do desafio 2

# o metodo process foi herdado e ja tem o esqueleto com a sequencia correta
class Billing::Payment::CreditCardOperation < SaveResourceOperationTemplate

  # agora basta implementar o método abaixo com as regras e o template method
  # criado em SaveResourceOperationTemplate
  # sabe o local e principalmente a sequencia de execucão
  def validade
    # O desenvolvedor cria o seguinte comentário abaixo

    # TODO: I’m not sure this is necessary, I just copied from a similar code

    # nesse caso se for necessário tratar problemas de concorrência
    # é interessante deixar essa responsabilidade para SaveResourceOperationTemplate
    # as classes filhas só conhecem o que deve ser validado
    # a classe mãe resolve problemas de concorrência
    authorization = authorize_credit_card
    antifraud = run_fraud_analysis(authorization)
    capture_credit_card if antifraud.success? && authorization.success?
  end

  # aqui é implementado outro método do template method
  # a classe filha pode definir tarefas ao final da execução do metodo process
  # uma prática ruby: melhor usar termos como "before_xyz", ao invez de "post_xyz"
  def before_process
    # irrelevant initialization code omitted (assume instance variables are initialized here)
    # Não existe mais a necessidade de registrar hooks na classe herdada,
    # a sequencia de processamento é definida no SaveResourceOperationTemplate
    Billing::Payment::EnsurePurchasePendingPayment.new
  end


  # Caso os metodos que fazem validacoes de fraude sejam encarados como
  # uma caracterestica em comum de qualquer "SaveResource"
  # esse metodos tambem podem ir para a classe mãe SaveResourceOperationTemplate
  private

  def authorize_credit_card
    Billing::Payment::Step::CCAuthorization.create(
      payment: @payment,
      api: payment_api
    )
  end

  def run_fraud_analysis(authorization)
    return fake_antifraud if antifraud_disabled?

    Billing::Payment::Step::AntiFraud.create(
      payment: @payment,
      api: antifraud_api,
      card: @credit_card_payment['card'],
      address: @address,
      client_ip: @client_ip,
      should_analyze: authorization.success?
    )
  end

  def capture_credit_card
    Billing::Payment::Step::CCCapture.create(
      payment: @payment,
      api: payment_api
    )
  end

  def payment_api
    @payment_api = PaymentGateway::Api.new(ENV.fetch('PAYMENT_API_TOKEN'))
  end

  def antifraud_api
    @antifraud_api = AntiFraud::Api.new(ENV.fetch('ANTIFRAUD_PRIVATE_KEY'))
  end

  def antifraud_disabled?
    ENV['DISABLE_ANTIFRAUD'] == 'true'
  end

  def fake_antifraud
    Billing::Payment::Step::FakeAntiFraud.create(
      payment: @payment
    )
  end
end
