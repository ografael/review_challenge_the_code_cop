# como foi falado no desafio
# o desenvolvedor também quer registrar os passos
# de cada ponto da transação.
# isso pode ser realizado dentro do template method
class SaveResourceOperationTemplate

  log = Logger.new(STDOUT)

  attr_reader :resource

  # Metodo principal de processamento
  # classes filhas não precisam reimplementar
  def process
    log.info("Iniciando processamento SaveResourceOperationTemplate em: #{self.class.name} ")
    # estrutura do template method
    Thread.new {
      log.info("Executando validação")
      validate
      log.info("#{self.class.name} está OK")
      resource.save
      log.info("#{self.class.name} foi salvo com id #{self.id}")
      before_process
    }
  end

  protected

  # metodos que definem as tarefas

  def before_process
    # pode ou não ser implementado pelas classes filhas
  end

  def validate
    # deve ser implamentado pelas classes filhas
    raise NotImplementedError
  end

end
