# Base, abstract class for any operation that leads to calling #save on a
# resource object. Provides hooks to be executed before and / or after the
# resource is saved...
class SaveResourceOperation
  attr_reader :resource, :before_hooks, :after_hooks

  # A hook is any instance that responds to #call. A before_hook will have its
  # #call method called before saving the instance.
  # The return value of this method call is not considered in any way, but
  # a before_hook that adds errors to the instance will prevent it from
  # being saved.
  #
  # after_hooks are similar, but #call is called only after every validator has
  # succeeded and the instance has successfully been saved.
  #
  # In both scenarios, a reference of the instance being processed is
  # passed as first and only argument to #call.
  def initialize(resource, opts = {}, *args)
    @resource = resource
    @before_hooks = opts.fetch(:before_hooks, [])
    @after_hooks = opts.fetch(:after_hooks, [])

    # Allow subclasses to specialize behavior without needing to override #initialize
    post_initialize(resource, opts, *args)
  end

  private

  # can be overridden by subclasses to customize behavior
  def post_initialize(resource, opts, *args); end

  # Can be overridden by subclasses to customize behavior
  def process
    resource.save
  end

  def validate_resource
    run_hooks(before_hooks)
    resource.errors.empty?
  end

  def run_hooks(hooks)
    hooks.each { |hook| hook.call(resource) }
  end
end



# Handles a credit card payment
class Billing::Payment::CreditCardOperation < SaveResourceOperation
  def post_initialize(payment, opts = {}, *_args)
    # irrelevant initialization code omitted (assume instance variables are initialized here)
    before_hooks.push Billing::Payment::EnsurePurchasePendingPayment.new
  end

  # Operation entry-point
  def process
    # TODO: I’m not sure this is necessary, I just copied from a similar code
    @payment.purchase.with_lock do
      authorization = authorize_credit_card
      antifraud = run_fraud_analysis(authorization)

      capture_credit_card if antifraud.success? && authorization.success?

      resource.save
    end
  end

  private

  def authorize_credit_card
    Billing::Payment::Step::CCAuthorization.create(
      payment: @payment,
      api: payment_api
    )
  end

  def run_fraud_analysis(authorization)
    return fake_antifraud if antifraud_disabled?

    Billing::Payment::Step::AntiFraud.create(
      payment: @payment,
      api: antifraud_api,
      card: @credit_card_payment['card'],
      address: @address,
      client_ip: @client_ip,
      should_analyze: authorization.success?
    )
  end

  def capture_credit_card
    Billing::Payment::Step::CCCapture.create(
      payment: @payment,
      api: payment_api
    )
  end

  def payment_api
    @payment_api = PaymentGateway::Api.new(ENV.fetch('PAYMENT_API_TOKEN'))
  end

  def antifraud_api
    @antifraud_api = AntiFraud::Api.new(ENV.fetch('ANTIFRAUD_PRIVATE_KEY'))
  end

  def antifraud_disabled?
    ENV['DISABLE_ANTIFRAUD'] == 'true'
  end

  def fake_antifraud
    Billing::Payment::Step::FakeAntiFraud.create(
      payment: @payment
    )
  end
end
