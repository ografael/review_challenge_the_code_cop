class CSVImportMemberships < CSVImportBase
  def initialize(file, venue, ignore_conflicts = true, quote_char = nil)
    @venue = venue
    # valor padrao de ignore_conflicts = true, isso vai depender da regra estabelecida;
    # não faz sentido a verificação true ou false que existia antes;
    @ignore_conflicts = ignore_conflicts
    super(file, quote_char)
  end

  # metodo herdado de CSVImportBase
  def valid_input?
    @errors << t('no_venue') unless @venue.is_a?(Venue)
    super
  end

  private

  # This method receives the data extracted from one row of the CSV file. It is called by the superclass
  # for each extracted row
  def process_row(params)
    # toda a responsabilidade de realizar o parse agora é encapsulada em outra classe
    # o parse é realizado para cada "row" conforme a regra estabelecida
    membership_parse = MembershipParse.new(params)

    # o processamento de salvar e tratar os erros de Membership
    # fica encapsulado no Parse.
    # created_count é um atributo de CSVImportBase,
    # só adiciona +1 caso o process_save do Parse seja "success"
    self.created_count += 1 if membership_parse.process_save?

    # invalid_rows é um atributo de CSVImportBase
    # caso exista alguma linha invalida é adicionada
    # ao invalid_rows da classe CSVImportBase
    # a partir do processamento do Parse,
    # que conhece como deve ser formatado essa informação
    invalid_rows << membership_parse.invalid_rows

  end

end
