class MembershipParse

  attr_accessor :membership, :params, :venue, :ignore_conflicts, :invalid_rows

  def initialize(params, veune, ignore_conflicts)
    # já é executado o sanitize_params na construção do Parse
    self.params = sanitize(params)
    # Como venue é passado pelo construtor de CVSImportMemberships e
    # tambem deve ser utilizado aqui, é criado uma referencia ao objeto
    self.venue = venue
    # parametro ignore_conflicts foi passado no construtor de CVSImportMemberships
    # e deve ser utilizado no Parse quando a classe MembershipTimeSanitizer é utilizada
    # Dessa forma apenas o Parse conhece MembershipTimeSanitizer, que agora está encapsulado aqui.
    self.ignore_conflicts = ignore_conflicts
    # instancia Membership a partir dos @params "sanitizados"
    # "to_params" melhor utilizar esse padrão de nomeclatura, conforme as boas práticas
    # ex: to_xml, to_json, to_etc...
    self.membership = Membership.new(to_params(self.params))
    # sempre iniciado vazio para cada execução
    self.invalid_rows = []
    # log utilizado na classe para registrar o log "fatal" em process_save
    @log = Logger.new(STDOUT)
  end

  # adicionado "?" no nome do metodo, seguinto as boas praticas ruby
  # nesse caso, pergunta se o process_save executou com sucesso
  def process_save?
    # considera o processamento sucesso caso um dos métodos abaixo seja true
    # evita a sequencia de if, else que existia
    return true if membership.save || membership.already_imported?

    # O padrão para para o encapsulamento das informações no atributo invalid_rows
    # é criado dentro do Parse, que depois é utilizado pela classe CVSImportMemberships.
    # Isso é interessante, caso você crie outra classe de importação ex: XMLImportMembership
    # pode retornar o invalid_rows do MemberhipParse e excutar outra regra de negócio.
    invalid_rows << {
      # agora pode referenciar diretamente o metodo privado "name", que retorna a string formadata
      name: name,
      membership: membership
    }

    return false

    # caso aconteça um erro, também é adicionado ao invalid_rows,
    # porém com o valor 'corrupted_data'
    # conforme a classe default do desafio
  rescue Exception => e
    # utiliza a mesma instância para retorno do invalid_rows
    # antes criava um novo objeto sem necessidade
    membership.errors.add('_', t('failed_to_process_data', data: params.to_s))
    invalid_rows << {
      name: t('corrupted_data'),
      membership: membership
    }
    # log fatal, dados corrompidos!
    @log.fatal { e }
  end

  private

  # metodos privados para membership_parse
  # melhora dos nomes para "to_params", faz mais sentido dentro de MemberhipParse
  # seguindo as boas práticas do ruby

  def to_params(params)
    # agora apenas a classe MemberhipParse conhece MembershipTimeSanitizer
    sanitizer = MembershipTimeSanitizer.new(params)
    {
      importing: true,
      import_data: params.dup,
      venue: venue,
      price: params[:price],
      start_time: sanitizer.membership_start_time,
      end_time: sanitizer.membership_end_time,
      ignore_overlapping_reservations: ignore_conflicts
    }
  end

  # melhorado os nomes dos metodos abaixo, existia uma redundância
  # time_description, date_description, etc_description
  # agora é encapsulado dentro de MembershipParse,
  # podendo ser acessado diretamente no contexto da classe MemberhipParse

  # não é necessário repassar params para cada metodo
  # params está encapsulado em MemberhipParse

  # esse método court_description, não está no codigo inicial.
  def name
    email = params[:email].present? ? params[:email] : t('no_email')
    "#{email}|#{date}|#{time}|" \
    "#{court_description(params)}|"
  end

  def date
    "#{params[:start_date]}-#{params[:end_date]}, #{params[:weekday]}"
  end

  def time
    "#{params[:start_time]}-#{params[:end_time]}"
  end

  def sanitize(params)
    # adicionado unless para evitar chamadas a uma referência .nil? == true
    params[:email] = params[:email].to_s.downcase unless params[:email].nil?
    params[:price] = params[:price].to_f unless params[:price].nil?
    params[:weekday] = params[:weekday].to_s.downcase unless params[:weekday].nil?
    params
  end

end
