class CSVImportMemberships < CSVImportBase
  def initialize(file, venue, ignore_conflicts, quote_char = nil)
    @venue = venue
    @ignore_conflicts = ignore_conflicts ? true : false

    super(file, quote_char)
  end

  def valid_input?
    @errors << t('no_venue') unless @venue.is_a?(Venue)

    super
  end

  private

  # This method receives the data extracted from one row of the CSV file. It is called by the superclass
  # for each extracted row
  def process_row(params)
    params = sanitize_params(params)
    membership = Membership.new(membership_params(params))
    if membership.save
      self.created_count += 1
    elsif membership.already_imported?
      self.skipped_count += 1
    else
      invalid_rows << {
        name: membership_name(params),
        membership: membership
      }
    end
  rescue Exception => e
    membership = Membership.new
    membership.errors.add('_', t('failed_to_process_data', data: params.to_s))
    invalid_rows << {
      name: t('corrupted_data'),
      membership: membership
    }
    p e.message
  end

  def membership_params(params)
    sanitizer = MembershipTimeSanitizer.new(params)

    {
      importing: true,
      import_data: params.dup,
      venue: @venue,
      price: params[:price],
      start_time: sanitizer.membership_start_time,
      end_time: sanitizer.membership_end_time,
      ignore_overlapping_reservations: @ignore_conflicts
    }
  end

  def membership_name(params)
    email = params[:email].present? ? params[:email] : t('no_email')
    "#{email}|#{date_description(params)}|#{time_description(params)}|" \
    "#{court_description(params)}|"
  end

  def date_description(params)
    "#{params[:start_date]}-#{params[:end_date]}, #{params[:weekday]}"
  end

  def time_description(params)
    "#{params[:start_time]}-#{params[:end_time]}"
  end

  def sanitize_params(params)
    params[:email] = params[:email].to_s.downcase
    params[:price] = params[:price].to_f
    params[:weekday] = params[:weekday].to_s.downcase

    params
  end
end
